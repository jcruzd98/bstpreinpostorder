/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package principal;

/**
 *
 * @author Josué
 */
public class BusquedaArbolBinario<T extends Comparable<T>> {

    private NodoArbolBinario<T> root;

    public boolean isEmpty() {
        return root == null;
    }

    public NodoArbolBinario<T> getRoot() {
        return root;
    }

    public boolean isRoot(NodoArbolBinario<T> nodo) {
        return root == nodo;
    }

    public boolean isLeaf(NodoArbolBinario<T> nodo) {
        return nodo.getLeft() == null && nodo.getRight() == null; //Determina si es una hoja     
    }

    public boolean isInternal(NodoArbolBinario<T> nodo) {
        return !isLeaf(nodo); //Devuelveme si no es una hoja
    }

    public NodoArbolBinario<T> add(NodoArbolBinario<T> origen, T elemento) { //añadir nodo recursivo
        NodoArbolBinario<T> nodo = null;
        //Si el root es nulo, lo añade el primero
        if (root == null)
        {
            nodo = new NodoArbolBinario<>(elemento);
            root = nodo;
        } else if (origen == null)
        { //el parametro pasado es invalido
            System.out.println("El origen es nulo");
        } else
        {

            //Comparamos los elementos
            //Si el nodo del origen es mayor que el elemento pasado, pasa a la izquierda
            if (origen.getElement().compareTo(elemento) > 0)
            {

                //Si tiene nodo izquierdo, hago la llamada recursiva
                if (origen.getLeft() != null)
                {
                    nodo = add(origen.getLeft(), elemento);
                } else
                {
                    //Creo el nodo
                    nodo = new NodoArbolBinario<>(elemento);
                    //Indico que el padre del nodo creado
                    nodo.setParent(origen);
                    //Indico que el nodo es el nodo izquierdo del origen
                    origen.setLeft(nodo);
                }

            } else
            { //sino pasa a la derecha

                if (origen.getRight() != null)
                {
                    nodo = add(origen.getRight(), elemento);
                } else
                {
                    nodo = new NodoArbolBinario<>(elemento);
                    nodo.setParent(origen);
                    origen.setRight(nodo);
                }

            }

        }

        return nodo;

    }//Fin método add recursivo

    public NodoArbolBinario<T> add(T elemento) { //añadir nodo de forma iterativa

        NodoArbolBinario<T> nodo = null;
        //Si el root es nulo, lo añade el primero
        if (root == null)
        {
            nodo = new NodoArbolBinario<>(elemento);
            root = nodo;
        } else
        {

            //Creo un nodo auxuliar
            NodoArbolBinario<T> aux = root;
            boolean insertado = false;
            //No salgo hasta que este insertado
            while (!insertado)
            {

                //Comparamos los elementos
                //Si el nodo del origen es mayor que el elemento pasado, pasa a la izquierda
                if (aux.getElement().compareTo(elemento) > 0)
                {

                    //Si tiene nodo izquierdo, actualizo el aux
                    if (aux.getLeft() != null)
                    {
                        aux = aux.getLeft();
                    } else
                    {
                        //Creo el nodo
                        nodo = new NodoArbolBinario<>(elemento);
                        //Indico que el padre del nodo creado
                        nodo.setParent(aux);
                        aux.setLeft(nodo);
                        //indico que lo he insertado
                        insertado = true;
                    }

                } else
                {

                    if (aux.getRight() != null)
                    {
                        aux = aux.getRight();
                    } else
                    {
                        nodo = new NodoArbolBinario<>(elemento);
                        nodo.setParent(aux);
                        aux.setRight(nodo);
                        insertado = true;
                    }

                }

            }

        }

        return nodo;

    }//Fin método add iterativo

    //Recorre los nodos, primero el padre y despues los hijos
    public void preorder(NodoArbolBinario<T> nodo) {

        System.out.println(nodo.getElement().toString());

        if (nodo.getLeft() != null)
        {
            preorder(nodo.getLeft());
        }

        if (nodo.getRight() != null)
        {
            preorder(nodo.getRight());
        }

    }

    //Recorre los nodos, lo recorre de izquierda a derecha
    public void inorder(NodoArbolBinario<T> nodo) {

        if (nodo.getLeft() != null)
        {
            inorder(nodo.getLeft());
        }

        System.out.println(nodo.getElement().toString());

        if (nodo.getRight() != null)
        {
            inorder(nodo.getRight());
        }

    }

    //Recorre los nodos, primero los hijos y luego el padre
    public void postorder(NodoArbolBinario<T> nodo) {

        if (nodo.getLeft() != null)
        {
            postorder(nodo.getLeft());
        }

        if (nodo.getRight() != null)
        {
            postorder(nodo.getRight());
        }

        System.out.println(nodo.getElement().toString());

    }

}//Fin clase
