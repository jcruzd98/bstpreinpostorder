/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package principal;

/**
 *
 * @author Josué
 */
public class principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Árbol Binario: PreOrder, InOrder, PostOrder

        BusquedaArbolBinario<Integer> arbol = new BusquedaArbolBinario<>();
        
        arbol.add(15);
        
        arbol.add(25);
        arbol.add(10);
        arbol.add(7);
        arbol.add(22);
        arbol.add(17);
        arbol.add(13);
        arbol.add(5);
        arbol.add(9);
        arbol.add(27);
        
        System.out.println("***** PreOrden *****");
        arbol.preorder(arbol.getRoot()); // 15, 10, 7, 5, 9, 13, 25, 22, 17, 27
        System.out.println("");
        
        System.out.println("***** InOrden *****");
        arbol.inorder(arbol.getRoot()); // 5, 7, 9, 10, 13, 15, 17, 22, 25, 27
        System.out.println("");
        
        System.out.println("***** PostOrden *****");
        arbol.postorder(arbol.getRoot());// 5, 9, 7, 13, 10, 17, 22, 27, 25, 15

    }
}//Fin Clase
